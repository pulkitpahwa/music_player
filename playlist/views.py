# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def create_playlist(request):
    """Create new playlist"""
    return HttpResponse("hello world")


def all_playlists(request):
    """get All playlists"""
    return HttpResponse("hello world")


def edit_playlist(request, id):
    """Edit playlist with the given id"""
    return HttpResponse("hello world")


def get_playlist(request, id):
    """Get playlist information and all songs of the playlist."""
    return HttpResponse("hello world")


def delete_playlist(request, id):
    """delete a playlist."""
    return HttpResponse("hello world")


def delete_song_from_playlist(request, id):
    """delete a song from playlist."""
    return HttpResponse("hello world")


def add_song_to_playlist(request, id):
    """add a song to playlist."""
    return HttpResponse("hello world")
