# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from songs.models import Song


class Playlist(models.Model):
    name_of_playlist = models.CharField(max_length=50)
    created_by = models.ForeignKey(User)

    def __str__(self):
        return self.name_of_playlist

class SongsOfPlaylist(models.Model):
    playlist = models.ForeignKey(Playlist)
    song = models.ForeignKey(Song)

    def __str__(self):
        return self.playlist.name_of_playlist

# Create your models here.
