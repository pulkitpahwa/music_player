from django.conf.urls import url
from .import views

urlpatterns = [
    url(r'^create$', views.create_playlist),
    url(r'^all$', views.all_playlists),
    url(r'^(?P<id>[-\d]+)/get$', views.get_playlist),
    url(r'^(?P<id>[-\d]+)/edit$', views.edit_playlist),
    url(r'^(?P<id>[-\d]+)/delete$', views.delete_playlist),

    url(r'^song/add$', views.add_song_to_playlist),
    url(r'^song/(?P<id>[-\d]+)/delete$',
        views.delete_song_from_playlist),
]
