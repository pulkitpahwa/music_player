Steps : 

1. Create a new project. (already created)
```django-admin.py startproject music_player```

2. cd to the project root.
```cd music_player```

3. Create a new app to store song information (already created)
```python manage.py startapp songs```

4. create a new app to store playlist information(already created)
```python manage.py startapp playlist```
