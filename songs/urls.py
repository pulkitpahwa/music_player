from django.conf.urls import url
from .import views

urlpatterns = [
    url(r'^add$', views.add_song),
    url(r'^(?P<id>[-\d]+)/edit$', views.edit_song),
    url(r'^(?P<id>[-\d]+)/get$', views.get_song),
    url(r'^(?P<id>[-\d]+)/delete$', views.delete_song),

]
