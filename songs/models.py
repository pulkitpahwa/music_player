# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Song(models.Model):
    name_of_song = models.CharField(max_length = 50)
    song_file = models.FileField(upload_to="songs")
    length = models.IntegerField(default=240)  # number of seconds
    uploaded_by = models.ForeignKey(User, blank=True, null=True)

    def __str__(self):
        return self.name_of_song

# Create your models here.
